# Python Week Of Code, Namibia 2019

Today,
we expect that CSS will work in diferent web browsers
and in different size of screens.
Write an effective CSS is a lot of work.
Many websites uses a CSS Frameworks, some examples are

- Bootstrap
- Foundation
- Bulma

In this course we will use Bulma.
For your project,
you are free to use your favourite.

## Task for Instructor

1. Add the responsive viewport meta tag

   ```
   <meta name="viewport" content="width=device-width, initial-scale=1">
   ```

   to `<head>`.
2. Add the CSS

   ```
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
   ```
3. A popular layout for website is to organise the information in columns. Add

   ```
   <div class="columns">
       <div class="column">
           First column
       </div>
       <div class="column">
           Second column
       </div>
       <div class="column">
           Third column
       </div>
   </div>
   ```
   
   On iPad, the website will render like

   ![](images/column-layout-ipad.png)

   and on iPhone

   ![](images/column-layout-iphone.png)
4. Is possible to define the size of the columns using classes. For example, change the previous snipet to

   ```
   <div class="columns">
       <div class="column is-half">
           First column
       </div>
       <div class="column is-one-quarter">
           Second column
       </div>
       <div class="column is-one-quarter">
           Third column
       </div>
   </div>
   ```

   Is also available a [12 columns system](https://bulma.io/documentation/columns/sizes/#12-columns-system).

   Note: By default, columns are only activated from tablet onwards.

   Note: You can also use `.is-centered` on the parent `.columns` element to center the columns.

   ```
   <div class="columns is-centered">
     <div class="column is-half">
     </div>
   </div>
   ```
5. Bulma define six main colors:

   - `is-primary`
   - `is-link`
   - `is-info`
   - `is-success`
   - `is-warning`
   - `is-danger`

   Three states:

   - `is-outlined`
   - `is-loading`
   - `[disabled]`

   You can use the colors and states to change the columns.
   For example,

   ```
   <div class="columns">
       <div class="column is-half is-info">
           First column
       </div>
       <div class="column is-one- is-success">
           Second column
       </div>
       <div class="column is-one-quarter is-danger">
           Third column
       </div>
   </div>
   ```
6. To center the text, you can use `.container`.
   For example,
   enclose the link to the blog posts inside `<div class="container">`.

## Tasks for Learners

1. Bulma supports [elements](https://bulma.io/documentation/elements).
   Add [tags](https://bulma.io/documentation/elements/tag) to the blog post page.
2. Bulma supports [components](https://bulma.io/documentation/components).
   Add [card](https://bulma.io/documentation/components/card) to the blog post page.